package aziz.sohail.cars.data.mapper

import aziz.sohail.cars.data.datastore.entity.CarEntity
import aziz.sohail.cars.domain.model.CarModel
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is
import org.junit.Before
import org.junit.Test

class CarEntityToCarModelMapperTest {

    private lateinit var carModel: CarModel

    @Before
    fun setup() {
        val carEntityToCarModelMapper = CarEntityToCarModelMapper()
        carModel = carEntityToCarModelMapper.map(CAR_ENTITY)
    }

    @Test
    fun `test name mapping`() {
        assertThat(carModel.name, Is.`is`(CAR_ENTITY.name))
    }

    @Test
    fun `test id mapping`() {
        assertThat(carModel.id, Is.`is`(CAR_ENTITY.id))
    }

    @Test
    fun `test modelIdentifies mapping`() {
        assertThat(carModel.modelIdentifier, Is.`is`(CAR_ENTITY.modelIdentifier))
    }

    @Test
    fun `test modelName mapping`() {
        assertThat(carModel.modelName, Is.`is`(CAR_ENTITY.modelName))
    }

    @Test
    fun `test make mapping`() {
        assertThat(carModel.make, Is.`is`(CAR_ENTITY.make))
    }

    @Test
    fun `test group mapping`() {
        assertThat(carModel.group, Is.`is`(CAR_ENTITY.group))
    }

    @Test
    fun `test color mapping`() {
        assertThat(carModel.color, Is.`is`(CAR_ENTITY.color))
    }

    @Test
    fun `test series mapping`() {
        assertThat(carModel.series, Is.`is`(CAR_ENTITY.series))
    }

    @Test
    fun `test fuelType mapping`() {
        assertThat(carModel.fuelType, Is.`is`(CAR_ENTITY.fuelType))
    }

    @Test
    fun `test fuelLevel mapping`() {
        assertThat(carModel.fuelLevel, Is.`is`(CAR_ENTITY.fuelLevel))
    }

    @Test
    fun `test transmission mapping`() {
        assertThat(carModel.transmission, Is.`is`(CAR_ENTITY.transmission))
    }

    @Test
    fun `test licensePlate mapping`() {
        assertThat(carModel.licensePlate, Is.`is`(CAR_ENTITY.licensePlate))
    }

    @Test
    fun `test latitude mapping`() {
        assertThat(carModel.latitude, Is.`is`(CAR_ENTITY.latitude))
    }

    @Test
    fun `test longitude mapping`() {
        assertThat(carModel.longitude, Is.`is`(CAR_ENTITY.longitude))
    }

    @Test
    fun `test innerCleanliness mapping`() {
        assertThat(carModel.innerCleanliness, Is.`is`(CAR_ENTITY.innerCleanliness))
    }

    @Test
    fun `test carImageUrl mapping`() {
        assertThat(carModel.carImageUrl, Is.`is`(CAR_ENTITY.carImageUrl))
    }

    companion object {
        val CAR_ENTITY = CarEntity(
            id = "WMWSW31030T222518",
            modelIdentifier = "mini",
            modelName = "MINI",
            name = "Vanessa",
            make = "BMW",
            group = "MINI",
            color = "midnight_black",
            series = "MINI",
            fuelType = "D",
            fuelLevel = 0.7f,
            transmission = "M",
            licensePlate = "M-VO0259",
            latitude = 48.134557,
            longitude = 11.576921,
            innerCleanliness = "REGULAR",
            carImageUrl = "https://cdn.sixt.io/codingtask/images/mini.png"
        )
    }

}