package aziz.sohail.cars.data.mapper

import aziz.sohail.cars.data.DefaultSerializerTest
import aziz.sohail.cars.data.datastore.entity.CarEntity
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is
import org.junit.Before
import org.junit.Test

class CarEntityJsonParsingTest : DefaultSerializerTest() {

    private lateinit var carEntity: CarEntity
    @Before
    fun testGetCarsResponseParsing() {

        carEntity = getJson(
            CarEntity::class.java,
            "response/get_cars_response.json"
        )

    }

    @Test
    fun `test car name parse correctly`() {
        assertThat(carEntity.name, Is.`is`("Vanessa"))
    }

    @Test
    fun `test car imageUrl parse correctly`() {
        assertThat(carEntity.carImageUrl, Is.`is`("https://cdn.sixt.io/codingtask/images/mini.png"))
    }

    @Test
    fun `test car InnerCleanliness parse correctly`() {
        assertThat(carEntity.innerCleanliness, Is.`is`("REGULAR"))
    }

    @Test
    fun `test car license plate parse correctly`() {
        assertThat(carEntity.licensePlate, Is.`is`("M-VO0259"))
    }

    @Test
    fun `test car transmission plate parse correctly`() {
        assertThat(carEntity.transmission, Is.`is`("M"))
    }

    @Test
    fun `test car fuel type parse correctly`() {
        assertThat(carEntity.fuelType, Is.`is`("D"))
    }

    @Test
    fun `test car series parse correctly`() {
        assertThat(carEntity.series, Is.`is`("MINI"))
    }

    @Test
    fun `test car color parse correctly`() {
        assertThat(carEntity.color, Is.`is`("midnight_black"))
    }

    @Test
    fun `test car make parse correctly`() {
        assertThat(carEntity.make, Is.`is`("BMW"))
    }

    @Test
    fun `test car group correctly`() {
        assertThat(carEntity.group, Is.`is`("MINI"))
    }

    @Test
    fun `test car modelName parse correctly`() {
        assertThat(carEntity.modelName, Is.`is`("MINI"))
    }

    @Test
    fun `test car modelIdentifier  parse correctly`() {
        assertThat(carEntity.modelIdentifier, Is.`is`("mini"))
    }

    @Test
    fun `test car id  parse correctly`() {
        assertThat(carEntity.id, Is.`is`("WMWSW31030T222518"))
    }

    @Test
    fun `test car fuelLevel  parse correctly`() {
        assertThat(carEntity.fuelLevel, Is.`is`(0.7f))
    }

    @Test
    fun `test car latitude  parse correctly`() {
        assertThat(carEntity.latitude, Is.`is`(48.134557))
    }

    @Test
    fun `test car longitude  parse correctly`() {
        assertThat(carEntity.longitude, Is.`is`(11.576921))
    }

}
