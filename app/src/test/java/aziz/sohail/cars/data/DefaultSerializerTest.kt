package aziz.sohail.cars.data

import com.squareup.moshi.Moshi
import okio.BufferedSource
import okio.Okio
import org.junit.Before
import java.io.IOException


open class DefaultSerializerTest {
    lateinit var moshi: Moshi

    @Before
    open fun setUp() {
        this.moshi = Moshi.Builder()
            .build()

    }

    fun getJson(filePath: String): BufferedSource {
        return Okio.buffer(Okio.source(javaClass.classLoader.getResourceAsStream(filePath)))
    }

    fun <T> getJson(clazz: Class<T>, filePath: String): T {
        val json = getJson(filePath)
        try {
            return moshi.adapter(clazz).fromJson(json)!!
        } catch (e: IOException) {
            e.printStackTrace()
            throw RuntimeException(e)
        }

    }
}
