package aziz.sohail.cars.data

import aziz.sohail.cars.data.datastore.entity.CarEntity
import aziz.sohail.cars.data.datastore.local.ILocalDataStore
import aziz.sohail.cars.data.datastore.remote.IApiDataStore
import aziz.sohail.cars.data.mapper.CarEntityToCarModelMapper
import aziz.sohail.cars.domain.ICarRepository
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

class CarRepositoryTest {

    private lateinit var carRepository: ICarRepository
    private val mockApiDataStore = mock(IApiDataStore::class.java)
    private val mockLocalStore = mock(ILocalDataStore::class.java)


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        carRepository = CarRepository(
            mockApiDataStore,
            mockLocalStore,
            mock(CarEntityToCarModelMapper::class.java)
        )
    }

    @Test
    fun `test cars stored in localDb after refresh`() {

        val carList: MutableList<CarEntity> = mutableListOf()

        Mockito.`when`(mockApiDataStore.getCars())
            .then { Single.just(carList) }


        carRepository.refreshCars().test()

        //verify local data store called
        Mockito.verify(mockLocalStore).storeCars(carList)
    }
}