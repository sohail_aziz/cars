package aziz.sohail.cars.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


@SuppressWarnings("UnusedReturnValue")
public class Precondition {
    Precondition() {
        //no instance
    }

    @NonNull
    public static <T> T checkNotNull(@Nullable T o) {
        if (o == null) {
            throw new IllegalArgumentException("must not be null");
        }
        return o;
    }

    public static int checkNotZeroOrNegative(int number) {
        if (number <= 0) {
            throw new IllegalArgumentException(String.format("number '%d' must not be zero or negative",
                    number
            ));
        }
        return number;
    }

    public static long checkNotZeroOrNegative(long number) {
        if (number <= 0) {
            throw new IllegalArgumentException(String.format("number '%d' must not be zero or negative",
                    number
            ));
        }
        return number;
    }

    public static float checkNotZeroOrNegative(float number) {
        if (number <= 0) {
            throw new IllegalArgumentException(String.format("number: '%f' must not be 0 or negative",
                    number
            ));
        }
        return number;
    }

    public static float checkNotNegative(float number) {
        if (number < 0) {
            throw new IllegalArgumentException(String.format("number: '%f' must not be negative",
                    number
            ));
        }
        return number;
    }

    public static long checkInRange(long start, long end, long number) {
        if (number < start || end < number) {
            throw new IllegalArgumentException(String.format("number: '%d' must not in range %d -> %d",
                    number,
                    start,
                    end
            ));
        }
        return number;
    }

    public static int checkInRange(int start, int end, int number) {
        if (number < start || end < number) {
            throw new IllegalArgumentException(String.format("number: '%d' must not in range %d -> %d",
                    number,
                    start,
                    end
            ));
        }
        return number;
    }

    public static double checkInRange(double start, double end, double number) {
        if (number < start || end < number) {
            throw new IllegalArgumentException(String.format("number: '%f' must not in range %f -> %f",
                    number,
                    start,
                    end
            ));
        }
        return number;
    }

    public static float checkInRange(float start, float end, float number) {
        if (number < start || end < number) {
            throw new IllegalArgumentException(String.format("number: '%f' must not in range %f -> %f",
                    number,
                    start,
                    end
            ));
        }
        return number;
    }

    public static double checkNotZeroOrNegative(double number) {
        if (number <= 0) {
            throw new IllegalArgumentException(String.format("number: '%f' must not be 0 or negative",
                    number
            ));
        }
        return number;
    }

    @NonNull
    public static String checkStringNotEmpty(@NonNull String string) {
        Precondition.checkNotNull(string);
        if (string.isEmpty()) {
            throw new IllegalArgumentException("String not expected to be empty.");
        }
        return string;
    }

    public static boolean checkAllEqual(int... numbers) {

        if (numbers.length == 1) {
            return true;
        }

        int previous = numbers[0];
        for (int i = 1, size = numbers.length; i < size; i++) {
            if (previous != numbers[i]) {
                throw new IllegalArgumentException("not equal");
            }

            previous = numbers[i];
        }
        return true;
    }

    public static void checkTrue(boolean assertedToBeTrue) {
        if (!assertedToBeTrue) {
            throw new IllegalStateException();
        }
    }
}
