package aziz.sohail.cars.domain.interactor

import aziz.sohail.cars.domain.ICarRepository
import aziz.sohail.cars.domain.executor.PostExecutionThread
import aziz.sohail.cars.domain.executor.ThreadExecutor
import aziz.sohail.cars.domain.interactor.base.ActivityLifecycleCompletableUseCase
import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class RefreshCarsUseCase
@Inject constructor(
    private val carRepository: ICarRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    activityEventObservable: Observable<ActivityEvent>?
) : ActivityLifecycleCompletableUseCase(
    threadExecutor,
    postExecutionThread,
    activityEventObservable
) {
    override fun buildUseCaseObservable(): Completable {
        return carRepository.refreshCars()
    }
}