package aziz.sohail.cars.domain.interactor.base;

import androidx.annotation.NonNull;

import aziz.sohail.cars.domain.Precondition;
import aziz.sohail.cars.domain.executor.PostExecutionThread;
import aziz.sohail.cars.domain.executor.ThreadExecutor;
import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class CompletableUseCase {

    @NonNull
    private final ThreadExecutor threadExecutor;
    @NonNull
    private final PostExecutionThread postExecutionThread;

    @NonNull
    private DisposableCompletableObserver subscription = new DefaultCompletableSubscriber();

    @SuppressWarnings("WeakerAccess")
    public CompletableUseCase(
            @NonNull ThreadExecutor threadExecutor, @NonNull PostExecutionThread postExecutionThread
    ) {
        this.threadExecutor = Precondition.checkNotNull(threadExecutor);
        this.postExecutionThread = Precondition.checkNotNull(postExecutionThread);
    }

    @NonNull
    public abstract Completable buildUseCaseObservable();

    public void execute(@NonNull DisposableCompletableObserver useCaseSubscriber) {
        Precondition.checkNotNull(useCaseSubscriber);
        subscription = useCaseSubscriber;
        getCompletable().subscribe(useCaseSubscriber);
    }

    public void unsubscribePreviousAndExecute(
            @NonNull DisposableCompletableObserver useCaseSubscriber
    ) {
        Precondition.checkNotNull(useCaseSubscriber);
        unsubscribe();
        execute(useCaseSubscriber);
    }

    @NonNull
    public Completable getCompletable() {
        return this.buildUseCaseObservable()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
    }

    /**
     * Unsubscribes from current {@link Disposable}.
     */
    @NonNull
    private CompletableUseCase unsubscribe() {
        if (!subscription.isDisposed()) {
            subscription.dispose();
        }
        return this;
    }
}
