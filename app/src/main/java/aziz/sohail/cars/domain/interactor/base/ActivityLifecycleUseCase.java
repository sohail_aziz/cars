package aziz.sohail.cars.domain.interactor.base;

import com.trello.rxlifecycle2.RxLifecycle;
import com.trello.rxlifecycle2.android.ActivityEvent;

import aziz.sohail.cars.domain.executor.PostExecutionThread;
import aziz.sohail.cars.domain.executor.ThreadExecutor;
import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by sohailaziz on 3/5/18.
 */

public abstract class ActivityLifecycleUseCase<T> extends UseCase<T> {
    private final Observable<ActivityEvent> activityEventObservable;
    private ActivityEvent activityEvent;

    public ActivityLifecycleUseCase(
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread,
            Observable<ActivityEvent> activityEventObservable
    ) {
        super(threadExecutor, postExecutionThread);
        this.activityEventObservable = activityEventObservable;
    }

    public ActivityLifecycleUseCase<T> unsubscribeOn(ActivityEvent activityEvent) {
        this.activityEvent = activityEvent;
        return this;
    }

    @Override
    public Observable<T> getObservable() {
        Observable<T> observable = super.getObservable();
        if (activityEvent != null) {
            observable =
                    observable.compose(RxLifecycle.bindUntilEvent(activityEventObservable, activityEvent));
        } else {
            Timber.e(new Exception(), "Call to getObservable() without specifying an unsubscribe event.");
        }
        return observable;
    }
}
