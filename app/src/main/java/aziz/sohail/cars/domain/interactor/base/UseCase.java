package aziz.sohail.cars.domain.interactor.base;

import androidx.annotation.NonNull;

import aziz.sohail.cars.domain.executor.PostExecutionThread;
import aziz.sohail.cars.domain.executor.ThreadExecutor;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture). This interfaces
 * represents a EXECUTION unit for different use cases (this means any use case in the application
 * should implement this contract). <p> By convention each UseCase implementation will return the
 * result using a rx.Subscriber that will execute its job in a background thread and will
 * post the result in the UI thread.
 */
public abstract class UseCase<T> {

    @NonNull
    private final ThreadExecutor threadExecutor;
    @NonNull
    private final PostExecutionThread postExecutionThread;

    private DisposableSubscriber<T> subscription = new DefaultSubscriber<>();

    UseCase(
            @NonNull ThreadExecutor threadExecutor, @NonNull PostExecutionThread postExecutionThread
    ) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
    }

    /**
     * Builds an  rx.Observable which will be used when executing the current {@link UseCase}.
     */
    public abstract Observable<T> buildUseCaseObservable();

    /**
     * Executes the current use case.
     *
     * @param useCaseSubscriber The guy who will be listen to the observable build with {@link
     *                          #buildUseCaseObservable()}.
     */
    public void execute(DisposableSubscriber<T> useCaseSubscriber) {
        subscription = getFlowable().subscribeWith(useCaseSubscriber);
    }

    public void unsubscribePreviousAndExecute(DisposableSubscriber<T> useCaseSubscriber) {
        unsubscribe();
        execute(useCaseSubscriber);
    }

    public Observable<T> getObservable() {
        return this.buildUseCaseObservable()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
    }

    private Flowable<T> getFlowable() {
        return getObservable().toFlowable(BackpressureStrategy.BUFFER);
    }

    /**
     * Unsubscribes from current {@link Disposable}.
     */
    private UseCase<T> unsubscribe() {
        if (!subscription.isDisposed()) {
            subscription.dispose();
        }
        return this;
    }
}
