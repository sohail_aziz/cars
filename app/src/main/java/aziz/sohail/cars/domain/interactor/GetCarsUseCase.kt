package aziz.sohail.cars.domain.interactor

import aziz.sohail.cars.domain.ICarRepository
import aziz.sohail.cars.domain.executor.PostExecutionThread
import aziz.sohail.cars.domain.executor.ThreadExecutor
import aziz.sohail.cars.domain.interactor.base.ActivityLifecycleUseCase
import aziz.sohail.cars.domain.model.CarModel
import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.Observable
import javax.inject.Inject

class GetCarsUseCase
@Inject constructor(
    private val carRepository: ICarRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    activityEventObservable: Observable<ActivityEvent>?
) : ActivityLifecycleUseCase<List<CarModel>>(
    threadExecutor,
    postExecutionThread,
    activityEventObservable
) {
    override fun buildUseCaseObservable(): Observable<List<CarModel>> {
        return carRepository.getCarsRx()
    }
}