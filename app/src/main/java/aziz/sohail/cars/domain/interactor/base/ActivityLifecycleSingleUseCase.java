package aziz.sohail.cars.domain.interactor.base;

import com.trello.rxlifecycle2.RxLifecycle;
import com.trello.rxlifecycle2.android.ActivityEvent;


import aziz.sohail.cars.domain.executor.PostExecutionThread;
import aziz.sohail.cars.domain.executor.ThreadExecutor;
import io.reactivex.Observable;
import io.reactivex.Single;
import timber.log.Timber;

/**
 * Created by sohailaziz on 3/5/18.
 */

public abstract class ActivityLifecycleSingleUseCase<T> extends SingleUseCase<T> {
  private final Observable<ActivityEvent> activityEventObservable;
  private ActivityEvent activityEvent;

  public ActivityLifecycleSingleUseCase(
      ThreadExecutor threadExecutor,
      PostExecutionThread postExecutionThread,
      Observable<ActivityEvent> activityEventObservable
  ) {
    super(threadExecutor, postExecutionThread);
    this.activityEventObservable = activityEventObservable;
  }

  public ActivityLifecycleSingleUseCase<T> unsubscribeOn(ActivityEvent activityEvent) {
    this.activityEvent = activityEvent;
    return this;
  }

  @Override public Single<T> getSingle() {
    Single<T> single = super.getSingle();
    if (activityEvent != null) {
      single = single.compose(RxLifecycle.bindUntilEvent(activityEventObservable, activityEvent));
    } else {
      Timber.e(new Exception(), "Call to getObservable() without specifying an unsubscribe event.");
    }
    return single;
  }
}
