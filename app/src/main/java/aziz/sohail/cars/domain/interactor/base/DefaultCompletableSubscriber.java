package aziz.sohail.cars.domain.interactor.base;

import io.reactivex.observers.DisposableCompletableObserver;
import timber.log.Timber;

public class DefaultCompletableSubscriber extends DisposableCompletableObserver {

  @Override protected void onStart() {
    super.onStart();
  }

  @Override public void onComplete() {
    // no-op by default.
  }

  @Override public void onError(Throwable e) {
    Timber.e(e);
  }
}
