package aziz.sohail.cars.domain

import aziz.sohail.cars.domain.model.CarModel
import io.reactivex.Completable
import io.reactivex.Observable

interface ICarRepository {

    fun refreshCars(): Completable
    fun getCarsRx(): Observable<List<CarModel>>
}