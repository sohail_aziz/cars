package aziz.sohail.cars.domain.interactor.base;

import com.trello.rxlifecycle2.RxLifecycle;
import com.trello.rxlifecycle2.android.ActivityEvent;

import aziz.sohail.cars.domain.executor.PostExecutionThread;
import aziz.sohail.cars.domain.executor.ThreadExecutor;
import io.reactivex.Completable;
import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by sohailaziz on 3/5/18.
 */

public abstract class ActivityLifecycleCompletableUseCase extends CompletableUseCase {
    private final Observable<ActivityEvent> activityEventObservable;
    private ActivityEvent activityEvent;

    public ActivityLifecycleCompletableUseCase(
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread,
            Observable<ActivityEvent> activityEventObservable
    ) {
        super(threadExecutor, postExecutionThread);
        this.activityEventObservable = activityEventObservable;
    }

    public ActivityLifecycleCompletableUseCase unsubscribeOn(ActivityEvent activityEvent) {
        this.activityEvent = activityEvent;
        return this;
    }

    @Override
    public Completable getCompletable() {
        Completable completable = super.getCompletable();
        if (activityEvent != null) {
            completable =
                    completable.compose(RxLifecycle.bindUntilEvent(activityEventObservable, activityEvent));
        } else {
            Timber.e(new Exception(), "Call to getObservable() without specifying an unsubscribe event.");
        }
        return completable;
    }
}
