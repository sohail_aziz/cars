package aziz.sohail.cars.presentation.ui.map.view

import aziz.sohail.cars.presentation.ui.base.IBaseCarView

interface IMapView : IBaseCarView {

    fun zoomToLocation(
        latitude: Double,
        longitude: Double
    )
}
