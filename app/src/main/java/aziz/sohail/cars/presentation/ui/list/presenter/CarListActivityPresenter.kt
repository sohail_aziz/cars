package aziz.sohail.cars.presentation.ui.list.presenter

import aziz.sohail.cars.domain.interactor.GetCarsUseCase
import aziz.sohail.cars.domain.interactor.RefreshCarsUseCase
import aziz.sohail.cars.domain.interactor.base.DefaultSubscriber
import aziz.sohail.cars.domain.model.CarModel
import aziz.sohail.cars.presentation.ui.base.BaseCarPresenter
import aziz.sohail.cars.presentation.ui.list.presenter.ICarListActivityPresenter
import aziz.sohail.cars.presentation.ui.list.view.ICarListView
import com.trello.rxlifecycle2.android.ActivityEvent
import javax.inject.Inject


class CarListActivityPresenter
@Inject constructor(
    refreshCarsUseCase: RefreshCarsUseCase,
    private val getCarsUseCase: GetCarsUseCase,
    private val view: ICarListView

) : BaseCarPresenter(refreshCarsUseCase, view), ICarListActivityPresenter {

    init {
        getCars()
    }

    private fun getCars() {
        getCarsUseCase
            .unsubscribeOn(ActivityEvent.DESTROY)
            .execute(object : DefaultSubscriber<List<CarModel>>() {
                override fun onStart() {
                    super.onStart()
                    view.showProgress()
                }

                override fun onError(throwable: Throwable) {
                    view.hideProgress()
                    view.onCarsLoadingFailed(throwable)
                }

                override fun onNext(carList: List<CarModel>) {
                    view.hideProgress()
                    view.onCarsLoaded(carList)
                }
            })
    }
}