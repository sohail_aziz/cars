package aziz.sohail.cars.presentation.ui.viewmodel

import aziz.sohail.cars.domain.model.CarModel

data class CarViewModel(val cardModel: CarModel)