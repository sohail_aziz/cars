package aziz.sohail.cars.presentation.internal.module

import aziz.sohail.cars.presentation.internal.PerActivity
import aziz.sohail.cars.presentation.ui.map.presenter.IMapActivityPresenter
import aziz.sohail.cars.presentation.ui.map.presenter.MapActivityPresenter
import aziz.sohail.cars.presentation.ui.map.view.IMapView
import aziz.sohail.cars.presentation.ui.map.view.MapsActivity
import dagger.Module
import dagger.Provides

@Module
class MapActivityModule(private val activity: MapsActivity) :
    BaseActivityModule(activity = activity) {

    @PerActivity
    @Provides
    fun provideView(): IMapView = activity

    @PerActivity
    @Provides
    fun provideCarListPresenter(presenter: MapActivityPresenter): IMapActivityPresenter = presenter

}