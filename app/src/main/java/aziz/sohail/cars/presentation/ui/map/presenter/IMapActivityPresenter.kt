package aziz.sohail.cars.presentation.ui.map.presenter

import aziz.sohail.cars.presentation.ui.base.IBaseCarPresenter

interface IMapActivityPresenter : IBaseCarPresenter {
    fun showCarList()
}