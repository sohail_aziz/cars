package aziz.sohail.cars.presentation.ui.list.view

import android.app.Application
import android.content.Context
import aziz.sohail.cars.presentation.internal.component.ApplicationComponent
import aziz.sohail.cars.presentation.internal.component.DaggerApplicationComponent
import timber.log.Timber

class CarsApplication : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent
            .builder()
            .carApplication(this)
            .build()

        Timber.plant(Timber.DebugTree())
    }

    companion object {
        fun getComponent(context: Context): ApplicationComponent {
            return (context as CarsApplication).applicationComponent;
        }

        fun getApplication(context: Context): CarsApplication {
            return (context as CarsApplication)
        }
    }
}