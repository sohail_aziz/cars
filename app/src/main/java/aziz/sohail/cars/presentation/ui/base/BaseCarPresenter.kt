package aziz.sohail.cars.presentation.ui.base

import aziz.sohail.cars.domain.interactor.RefreshCarsUseCase
import aziz.sohail.cars.domain.interactor.base.DefaultCompletableSubscriber
import com.trello.rxlifecycle2.android.ActivityEvent

open class BaseCarPresenter
constructor(
    private val refreshCarsUseCase: RefreshCarsUseCase,
    private val view: IBaseCarView
) : IBaseCarPresenter {

    override fun onResume() {
        refreshCars()
    }

    override fun refreshCars() {
        refreshCarsUseCase
            .unsubscribeOn(ActivityEvent.DESTROY)
            .unsubscribePreviousAndExecute(object : DefaultCompletableSubscriber() {
                override fun onStart() {
                    super.onStart()
                    view.showProgress()
                }

                override fun onError(e: Throwable) {
                    view.hideProgress()
                    view.onCarsLoadingFailed(e)
                }

                override fun onComplete() {
                    view.hideProgress()
                }
            })
    }
}