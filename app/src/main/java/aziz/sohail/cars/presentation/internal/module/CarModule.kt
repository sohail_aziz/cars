package aziz.sohail.cars.presentation.internal.module

import android.content.Context
import androidx.room.Room
import aziz.sohail.cars.data.CarRepository
import aziz.sohail.cars.data.datastore.local.CarDao
import aziz.sohail.cars.data.datastore.local.CarDatabase
import aziz.sohail.cars.data.datastore.local.ILocalDataStore
import aziz.sohail.cars.data.datastore.local.RoomDataStore
import aziz.sohail.cars.data.datastore.remote.ApiDataStore
import aziz.sohail.cars.data.datastore.remote.CarApiService
import aziz.sohail.cars.data.datastore.remote.IApiDataStore
import aziz.sohail.cars.domain.ICarRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class CarModule {

    @Singleton
    @Provides
    fun provideCarRepository(carRepository: CarRepository): ICarRepository {
        return carRepository
    }

    @Singleton
    @Provides
    fun provideCarLocalDataStore(roomDataStore: RoomDataStore): ILocalDataStore = roomDataStore


    @Singleton
    @Provides
    fun provideCarApiDataStore(carApiDataStore: ApiDataStore): IApiDataStore = carApiDataStore

    @Singleton
    @Provides
    fun provideCarDao(carDatabase: CarDatabase): CarDao {
        return carDatabase.createCarDao()
    }

    @Singleton
    @Provides
    fun provideCarApiService(retrofit: Retrofit): CarApiService {
        return retrofit.create(CarApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideRoomDatabase(
        context: Context
    ): CarDatabase {
        return Room.databaseBuilder(context, CarDatabase::class.java, "cars.db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }


}