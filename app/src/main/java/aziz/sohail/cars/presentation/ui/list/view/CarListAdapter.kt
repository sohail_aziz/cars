package aziz.sohail.cars.presentation.ui.list.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import aziz.sohail.cars.R
import aziz.sohail.cars.domain.model.CarModel
import aziz.sohail.cars.presentation.ui.viewmodel.CarViewModel
import com.squareup.picasso.Picasso
import javax.inject.Inject

class CarListAdapter
@Inject constructor() :
    RecyclerView.Adapter<CarListAdapter.CarViewHolder>() {

    private var carList = listOf<CarViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(CarViewHolder.LAYOUT, parent, false)
        return CarViewHolder(view)
    }

    override fun getItemCount(): Int {
        return carList.size
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.bind(carList[position])
    }

    fun setData(carModels: List<CarModel>) {

        val newCarList = carModels.map {
            CarViewModel(it)
        }

        val diffCallback = CarDiffCallback(carList, newCarList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.carList = newCarList

        diffResult.dispatchUpdatesTo(this)

    }


    //view holder
    class CarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val nameView: TextView = itemView.findViewById(R.id.text_view_car_name)
        private val colorView: TextView = itemView.findViewById(R.id.text_view_color)
        private val licensePlatView: TextView = itemView.findViewById(R.id.text_view_license_plate)
        private val iconView: ImageView = itemView.findViewById(R.id.image_view_car)

        fun bind(carViewModel: CarViewModel) {

            val carModel = carViewModel.cardModel
            nameView.text = String.format(
                itemView.context.getString(R.string.car_name),
                carModel.make,
                carModel.name
            )

            colorView.text =
                String.format(itemView.context.getString(R.string.car_color), carModel.color)

            licensePlatView.text = String.format(
                itemView.context.getString(
                    R.string.car_license,
                    carModel.licensePlate
                )
            )

            Picasso.with(itemView.context)
                .load(carModel.carImageUrl)
                .placeholder(R.drawable.ic_car_marker)
                .into(iconView)

        }

        companion object {
            const val LAYOUT = R.layout.list_item_car
        }
    }
}

//diff callback
class CarDiffCallback
constructor(
    private val oldList: List<CarViewModel>,
    private val newList: List<CarViewModel>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].cardModel.id == newList[newItemPosition].cardModel.id
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].cardModel == newList[newItemPosition].cardModel
    }
}