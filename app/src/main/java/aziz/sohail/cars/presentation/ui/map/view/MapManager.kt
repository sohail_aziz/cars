package aziz.sohail.cars.presentation.ui.map.view

import aziz.sohail.cars.R
import aziz.sohail.cars.domain.model.CarModel
import aziz.sohail.cars.presentation.base.Settings
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

object MapManager {

    fun addMarkers(googleMap: GoogleMap, cars: List<CarModel>) {

        googleMap.clear()

        for (car in cars) {

            val latLng = LatLng(car.latitude, car.longitude)
            val markerOptions = MarkerOptions()
            markerOptions.position(latLng)
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_marker))

            googleMap.addMarker(markerOptions)

        }

    }

    fun zoomTo(googleMap: GoogleMap, latLng: LatLng) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, Settings.Map.ZOOM_LEVEL))
    }
}