package aziz.sohail.cars.presentation.internal.module

import aziz.sohail.cars.presentation.internal.PerActivity
import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import dagger.Module
import dagger.Provides
import io.reactivex.Observable

@Module
open class BaseActivityModule(private val activity: RxAppCompatActivity) {

    @PerActivity
    @Provides
    fun provideActivityLifecycle(): Observable<ActivityEvent> {
        return activity.lifecycle()
    }

    @PerActivity
    @Provides
    fun provideActivity(): RxAppCompatActivity {
        return activity
    }
}