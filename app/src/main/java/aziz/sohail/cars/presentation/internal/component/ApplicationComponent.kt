package aziz.sohail.cars.presentation.internal.component

import aziz.sohail.cars.presentation.internal.module.ApplicationModule
import aziz.sohail.cars.presentation.internal.module.CarModule
import aziz.sohail.cars.presentation.internal.module.NetworkModule
import aziz.sohail.cars.presentation.ui.list.view.CarsApplication
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class, CarModule::class])
interface ApplicationComponent {

    //injection point
    fun inject(application: CarsApplication)

    //subComponents
    fun carListActivityComponentBuilder(): CarListActivityComponent.Builder

    fun mapActivityComponentBuilder(): MapActivityComponent.Builder


    @Component.Builder
    interface Builder {
        @BindsInstance
        fun carApplication(application: CarsApplication): Builder

        fun build(): ApplicationComponent
    }
}