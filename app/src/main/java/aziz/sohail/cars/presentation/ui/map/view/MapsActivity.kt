package aziz.sohail.cars.presentation.ui.map.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import aziz.sohail.cars.R
import aziz.sohail.cars.domain.model.CarModel
import aziz.sohail.cars.presentation.internal.component.MapActivityComponent
import aziz.sohail.cars.presentation.internal.module.MapActivityModule
import aziz.sohail.cars.presentation.ui.ErrorMessageFactory
import aziz.sohail.cars.presentation.ui.base.hide
import aziz.sohail.cars.presentation.ui.base.show
import aziz.sohail.cars.presentation.ui.list.view.CarsApplication
import aziz.sohail.cars.presentation.ui.map.presenter.IMapActivityPresenter
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import kotlinx.android.synthetic.main.activity_maps.*
import javax.inject.Inject

class MapsActivity : RxAppCompatActivity(), IMapView, OnMapReadyCallback {


    @Inject
    lateinit var presenter: IMapActivityPresenter

    private var googleMap: GoogleMap? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        getInjector().inject(this)

    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
    }

    override fun onCarsLoaded(cars: List<CarModel>) {

        if (googleMap != null) {
            MapManager.addMarkers(googleMap!!, cars)
        }
    }

    override fun zoomToLocation(latitude: Double, longitude: Double) {

        if (googleMap != null) {
            MapManager.zoomTo(
                googleMap = googleMap!!,
                latLng = LatLng(latitude, longitude)
            )
        }


    }

    override fun showProgress() {
        progress_bar_loading.show()
    }

    override fun hideProgress() {
        progress_bar_loading.hide()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.map, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.menu_show_list) {
            presenter.showCarList()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCarsLoadingFailed(throwable: Throwable) {

        val snackBar =
            Snackbar.make(
                root_view,
                ErrorMessageFactory.getErrorMessage(throwable),
                Snackbar.LENGTH_INDEFINITE
            )

        snackBar.setAction(R.string.action_retry) {
            presenter.refreshCars()
        }

        snackBar.show()

    }

    private fun getInjector(): MapActivityComponent {

        return CarsApplication.getComponent(applicationContext)
            .mapActivityComponentBuilder()
            .mapActivityModule(MapActivityModule(this))
            .build();
    }
}
