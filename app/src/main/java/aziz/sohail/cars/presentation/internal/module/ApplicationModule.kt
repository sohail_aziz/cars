package aziz.sohail.cars.presentation.internal.module

import android.content.Context
import aziz.sohail.cars.data.executor.JobExecutor
import aziz.sohail.cars.domain.executor.PostExecutionThread
import aziz.sohail.cars.domain.executor.ThreadExecutor
import aziz.sohail.cars.presentation.base.UIThread
import aziz.sohail.cars.presentation.internal.component.CarListActivityComponent
import aziz.sohail.cars.presentation.internal.component.MapActivityComponent
import aziz.sohail.cars.presentation.ui.list.view.CarsApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(subcomponents = [CarListActivityComponent::class, MapActivityComponent::class])
class ApplicationModule {

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    fun provideApplicationContext(application: CarsApplication): Context =
        application.applicationContext
}