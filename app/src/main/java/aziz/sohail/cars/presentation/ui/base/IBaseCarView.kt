package aziz.sohail.cars.presentation.ui.base

import aziz.sohail.cars.domain.model.CarModel

interface IBaseCarView:ILoadingView {
    fun onCarsLoaded(cars: List<CarModel>)
    fun onCarsLoadingFailed(throwable: Throwable)
}