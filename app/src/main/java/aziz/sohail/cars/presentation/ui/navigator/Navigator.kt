package aziz.sohail.cars.presentation.ui.navigator

import aziz.sohail.cars.presentation.ui.list.view.CarListActivity
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import javax.inject.Inject

class Navigator
@Inject constructor(
    private val activity: RxAppCompatActivity
) {

    fun startCarListActivity() {
        activity.startActivity(CarListActivity.getCallingIntent(activity))
    }
}