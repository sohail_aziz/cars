package aziz.sohail.cars.presentation.ui.base

import android.view.View

fun View.show(){
    this.visibility=View.VISIBLE
}

fun View.hide() {
    this.visibility= View.GONE
}