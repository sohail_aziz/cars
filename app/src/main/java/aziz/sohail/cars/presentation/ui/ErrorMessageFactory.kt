package aziz.sohail.cars.presentation.ui

import androidx.annotation.StringRes
import aziz.sohail.cars.R
import java.io.IOException

object ErrorMessageFactory {

    @StringRes
    fun getErrorMessage(throwable: Throwable): Int {

        return when (throwable) {
            is IOException -> R.string.error_offline
            else -> R.string.error_unknown
        }

    }
}