package aziz.sohail.cars.presentation.internal.component

import aziz.sohail.cars.presentation.internal.PerActivity
import aziz.sohail.cars.presentation.internal.module.CarListActivityModule
import aziz.sohail.cars.presentation.ui.list.view.CarListActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [CarListActivityModule::class])
interface CarListActivityComponent {


    //MainActivityComponent builder
    @Subcomponent.Builder
    interface Builder {
        fun carListActivityModule(module: CarListActivityModule): CarListActivityComponent.Builder

        fun build(): CarListActivityComponent
    }

    fun inject(activity: CarListActivity)
}