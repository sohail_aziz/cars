package aziz.sohail.cars.presentation.internal.component

import aziz.sohail.cars.presentation.internal.PerActivity
import aziz.sohail.cars.presentation.internal.module.MapActivityModule
import aziz.sohail.cars.presentation.ui.map.view.MapsActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [MapActivityModule::class])
interface MapActivityComponent {


    @Subcomponent.Builder
    interface Builder {
        fun mapActivityModule(module: MapActivityModule): MapActivityComponent.Builder

        fun build(): MapActivityComponent
    }

    fun inject(activity: MapsActivity)
}