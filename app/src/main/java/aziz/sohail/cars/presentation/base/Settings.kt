package aziz.sohail.cars.presentation.base

object Settings {

    const val API_BASE_URL = "https://cdn.sixt.io"

    object Map {
        const val ZOOM_LEVEL = 11.0f
    }

}