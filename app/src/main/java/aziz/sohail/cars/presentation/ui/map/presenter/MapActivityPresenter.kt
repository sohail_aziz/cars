package aziz.sohail.cars.presentation.ui.map.presenter

import aziz.sohail.cars.domain.interactor.GetCarsUseCase
import aziz.sohail.cars.domain.interactor.RefreshCarsUseCase
import aziz.sohail.cars.domain.interactor.base.DefaultSubscriber
import aziz.sohail.cars.domain.model.CarModel
import aziz.sohail.cars.presentation.ui.base.BaseCarPresenter
import aziz.sohail.cars.presentation.ui.map.view.IMapView
import aziz.sohail.cars.presentation.ui.navigator.Navigator
import com.trello.rxlifecycle2.android.ActivityEvent
import javax.inject.Inject

class MapActivityPresenter
@Inject constructor(
    private val navigator: Navigator,
    refreshCarsUseCase: RefreshCarsUseCase,
    private val getCarsUseCase: GetCarsUseCase,
    private val view: IMapView
) : BaseCarPresenter(refreshCarsUseCase, view), IMapActivityPresenter {

    init {
        getCars()
    }

    private fun getCars() {
        getCarsUseCase
            .unsubscribeOn(ActivityEvent.DESTROY)
            .execute(object : DefaultSubscriber<List<CarModel>>() {
                override fun onStart() {
                    super.onStart()
                    view.showProgress()
                }

                override fun onError(throwable: Throwable) {
                    view.hideProgress()
                    view.onCarsLoadingFailed(throwable)
                }

                override fun onNext(carList: List<CarModel>) {
                    view.hideProgress()
                    view.onCarsLoaded(carList)

                    if (carList.isNotEmpty()) {
                        val lastCar = carList.last()
                        view.zoomToLocation(lastCar.latitude, lastCar.longitude)
                    }

                }
            })
    }


    override fun showCarList() {
        navigator.startCarListActivity()
    }
}