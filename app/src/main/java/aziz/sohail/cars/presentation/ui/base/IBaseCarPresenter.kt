package aziz.sohail.cars.presentation.ui.base

interface IBaseCarPresenter {
    fun onResume()
    fun refreshCars()
}