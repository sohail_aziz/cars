package aziz.sohail.cars.presentation.internal.module

import aziz.sohail.cars.presentation.internal.PerActivity
import aziz.sohail.cars.presentation.ui.list.presenter.CarListActivityPresenter
import aziz.sohail.cars.presentation.ui.list.presenter.ICarListActivityPresenter
import aziz.sohail.cars.presentation.ui.list.view.CarListActivity
import aziz.sohail.cars.presentation.ui.list.view.ICarListView
import dagger.Module
import dagger.Provides

@Module
class CarListActivityModule(private val activity: CarListActivity) :
    BaseActivityModule(activity = activity) {

    @PerActivity
    @Provides
    fun provideCarListView(): ICarListView = activity

    @PerActivity
    @Provides
    fun provideCarListPresenter(presenter: CarListActivityPresenter): ICarListActivityPresenter =
        presenter

}