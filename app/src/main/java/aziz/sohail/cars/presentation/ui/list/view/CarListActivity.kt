package aziz.sohail.cars.presentation.ui.list.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import aziz.sohail.cars.R
import aziz.sohail.cars.domain.model.CarModel
import aziz.sohail.cars.presentation.internal.component.CarListActivityComponent
import aziz.sohail.cars.presentation.internal.module.CarListActivityModule
import aziz.sohail.cars.presentation.ui.ErrorMessageFactory
import aziz.sohail.cars.presentation.ui.base.hide
import aziz.sohail.cars.presentation.ui.base.show
import aziz.sohail.cars.presentation.ui.list.presenter.ICarListActivityPresenter
import com.google.android.material.snackbar.Snackbar
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class CarListActivity : RxAppCompatActivity(), ICarListView {

    @Inject
    lateinit var presenter: ICarListActivityPresenter

    @Inject
    lateinit var carAdapter: CarListAdapter

    private lateinit var recyclerViewCars: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getInjector().inject(this)

        initView()
    }

    private fun initView() {
        recyclerViewCars = findViewById(R.id.recycler_view_cars)
        recyclerViewCars.layoutManager = LinearLayoutManager(this)
        recyclerViewCars.adapter = carAdapter

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onCarsLoaded(cars: List<CarModel>) {
        carAdapter.setData(cars)
    }

    override fun onCarsLoadingFailed(throwable: Throwable) {
        val snackBar =
            Snackbar.make(
                list_root_view,
                ErrorMessageFactory.getErrorMessage(throwable),
                Snackbar.LENGTH_INDEFINITE
            )

        snackBar.setAction(R.string.action_retry, View.OnClickListener {
            presenter.refreshCars()
        })

        snackBar.show()
    }

    override fun showProgress() {
        progress_bar_loading.show()
    }

    override fun hideProgress() {
        progress_bar_loading.hide()
    }

    private fun getInjector(): CarListActivityComponent {

        return CarsApplication.getComponent(applicationContext)
            .carListActivityComponentBuilder()
            .carListActivityModule(CarListActivityModule(this))
            .build();
    }

    companion object {

        fun getCallingIntent(context: Context): Intent {
            return Intent(context, CarListActivity::class.java)
        }
    }


}
