package aziz.sohail.cars.presentation.ui.base

interface ILoadingView {
    fun showProgress()
    fun hideProgress()
}