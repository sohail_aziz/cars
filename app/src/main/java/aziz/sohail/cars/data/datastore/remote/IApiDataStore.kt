package aziz.sohail.cars.data.datastore.remote

import aziz.sohail.cars.data.datastore.entity.CarEntity
import io.reactivex.Single

interface IApiDataStore {

    fun getCars(): Single<List<CarEntity>>
}