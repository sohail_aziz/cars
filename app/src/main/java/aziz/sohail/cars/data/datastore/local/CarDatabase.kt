package aziz.sohail.cars.data.datastore.local

import androidx.room.Database
import androidx.room.RoomDatabase
import aziz.sohail.cars.data.datastore.entity.CarEntity
import aziz.sohail.cars.presentation.base.Constants

@Database(
    version = Constants.DatabaseVersion,
    entities = [CarEntity::class]
)
abstract class CarDatabase : RoomDatabase() {
    abstract fun createCarDao(): CarDao
}