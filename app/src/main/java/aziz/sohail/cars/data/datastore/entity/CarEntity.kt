package aziz.sohail.cars.data.datastore.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@Entity(tableName = "table_cars")
@JsonClass(generateAdapter = true)
data class CarEntity(
    @PrimaryKey
    @Json(name = "id") val id: String,
    @Json(name = "modelIdentifier") val modelIdentifier: String,
    @Json(name = "modelName") val modelName: String,
    @Json(name = "name") val name: String,
    @Json(name = "make") val make: String,
    @Json(name = "group") val group: String,
    @Json(name = "color") val color: String,
    @Json(name = "series") val series: String,
    @Json(name = "fuelType") val fuelType: String,
    @Json(name = "fuelLevel") val fuelLevel: Float,
    @Json(name = "transmission") val transmission: String,
    @Json(name = "licensePlate") val licensePlate: String,
    @Json(name = "latitude") val latitude: Double,
    @Json(name = "longitude") val longitude: Double,
    @Json(name = "innerCleanliness") val innerCleanliness: String,
    @Json(name = "carImageUrl") val carImageUrl: String
)