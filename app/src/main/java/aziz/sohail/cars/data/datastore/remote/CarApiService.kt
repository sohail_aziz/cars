package aziz.sohail.cars.data.datastore.remote

import aziz.sohail.cars.data.datastore.entity.CarEntity
import io.reactivex.Single
import retrofit2.http.GET

interface CarApiService {

    @GET("codingtask/cars")
    fun getCars(): Single<List<CarEntity>>
}