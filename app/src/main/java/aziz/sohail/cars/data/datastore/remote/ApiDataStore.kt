package aziz.sohail.cars.data.datastore.remote

import aziz.sohail.cars.data.datastore.entity.CarEntity
import io.reactivex.Single
import javax.inject.Inject

class ApiDataStore
@Inject constructor(
    private val carApiService: CarApiService
) : IApiDataStore {

    override fun getCars(): Single<List<CarEntity>> {
        return carApiService.getCars()
    }
}