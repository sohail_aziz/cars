package aziz.sohail.cars.data

import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named


@Named(LoggingInterceptor.NAME)
class LoggingInterceptor @Inject internal constructor() : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val t1 = System.currentTimeMillis()
        Timber.i("-> %s %s", request.method(), request.url())
        Timber.d(request.cacheControl().toString())
        for (name in request.headers().names()) {
            Timber.d(request.header(name))
        }
        val response = chain.proceed(request)
        val t2 = System.currentTimeMillis()
        Timber.i("<- %s : %d in %d ms %n ", response.request().url(), response.code(), t2 - t1)
        return response
    }

    companion object {
        const val NAME = "LoggingInterceptor"
    }
}