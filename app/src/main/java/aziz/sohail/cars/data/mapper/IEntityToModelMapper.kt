package aziz.sohail.cars.data.mapper


interface IEntityToModelMapper<in In, out Out> {

    fun map(dataToTransform: In): Out

    fun map(dataList: List<In>): List<Out> {
        return dataList.map {
            map(it)
        }
    }

}