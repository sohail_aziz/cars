package aziz.sohail.cars.data.datastore.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import aziz.sohail.cars.data.datastore.entity.CarEntity
import io.reactivex.Flowable

@Dao
interface CarDao {

    @Query("Select * from table_cars")
    fun getCarsRx(): Flowable<List<CarEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addOrUpdateCars(cars: List<CarEntity>)
}