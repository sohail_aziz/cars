package aziz.sohail.cars.data.datastore.local

import aziz.sohail.cars.data.datastore.entity.CarEntity
import io.reactivex.Completable
import io.reactivex.Observable

interface ILocalDataStore {

    fun getCarsRx(): Observable<List<CarEntity>>

    fun storeCars(cars: List<CarEntity>): Completable
}