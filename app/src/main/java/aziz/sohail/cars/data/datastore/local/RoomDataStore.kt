package aziz.sohail.cars.data.datastore.local

import aziz.sohail.cars.data.datastore.entity.CarEntity
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class RoomDataStore
@Inject constructor(
    private val carDao: CarDao
) : ILocalDataStore {

    override fun getCarsRx(): Observable<List<CarEntity>> {
        return carDao.getCarsRx()
            .toObservable()

    }

    override fun storeCars(cars: List<CarEntity>): Completable {
        carDao.addOrUpdateCars(cars = cars)
        return Completable.complete()
    }
}