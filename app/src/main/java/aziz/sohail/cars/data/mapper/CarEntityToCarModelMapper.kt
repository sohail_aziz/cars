package aziz.sohail.cars.data.mapper

import aziz.sohail.cars.data.datastore.entity.CarEntity
import aziz.sohail.cars.domain.model.CarModel
import javax.inject.Inject

class CarEntityToCarModelMapper
@Inject constructor() : IEntityToModelMapper<CarEntity, CarModel> {

    override fun map(dataToTransform: CarEntity): CarModel {
        return CarModel(
            id = dataToTransform.id,
            modelIdentifier = dataToTransform.modelIdentifier,
            modelName = dataToTransform.modelName,
            name = dataToTransform.name,
            group = dataToTransform.group,
            make = dataToTransform.make,
            color = dataToTransform.color,
            series = dataToTransform.series,
            fuelType = dataToTransform.fuelType,
            fuelLevel = dataToTransform.fuelLevel,
            transmission = dataToTransform.transmission,
            licensePlate = dataToTransform.licensePlate,
            latitude = dataToTransform.latitude,
            longitude = dataToTransform.longitude,
            innerCleanliness = dataToTransform.innerCleanliness,
            carImageUrl = dataToTransform.carImageUrl
        )
    }
}