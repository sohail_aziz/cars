package aziz.sohail.cars.data

import aziz.sohail.cars.data.datastore.local.ILocalDataStore
import aziz.sohail.cars.data.datastore.remote.ApiDataStore
import aziz.sohail.cars.data.datastore.remote.IApiDataStore
import aziz.sohail.cars.data.mapper.CarEntityToCarModelMapper
import aziz.sohail.cars.domain.ICarRepository
import aziz.sohail.cars.domain.model.CarModel
import io.reactivex.Completable
import io.reactivex.Observable
import timber.log.Timber
import javax.inject.Inject

class CarRepository
@Inject constructor(
    private val apiDataStore: IApiDataStore,
    private val localDataStore: ILocalDataStore,
    private val carEntityToModelMapper: CarEntityToCarModelMapper
) : ICarRepository {
    override fun refreshCars(): Completable {
        return apiDataStore.getCars()
            .doOnSuccess {
                localDataStore.storeCars(it)
            }
            .ignoreElement()

    }

    override fun getCarsRx(): Observable<List<CarModel>> {
        return localDataStore.getCarsRx()
            .map {
                carEntityToModelMapper.map(it)
            }
    }
}